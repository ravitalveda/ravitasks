﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FirstAssignment
{
    public class Program
    {
        static void Main(string[] args)
        {
            GameOfStones gs = new GameOfStones();
            int[] stones = { 2, 4, 4, 6 };
            int noOfStones = gs.Count(stones);
            Console.WriteLine(noOfStones);
            Console.ReadKey();
        }
    }

    public class GameOfStones
    {
        public bool countOdds(int[] values)
        {
            int countOdd = 0;
            int countEven = 0;
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] % 2 != 0)
                {
                    countOdd++;
                }
                else
                {
                    countEven++;
                }
            }
            //returns if even numbers and odd numbers doesn't match
            //e.g, if 4 piles having even numbers and 3 piles having odd numbers cannot be eaqualized
            if ((Math.Abs(countOdd - countEven)) % 2 == 0)
                return true;
            else
                return false;
        }

        public int Count(int[] stones)
        {
            int avg = (int)stones.Average();

            // Check if each pile can have same number of stones.
            bool isNotEqual = Convert.ToBoolean(stones.Sum() % avg);

            bool isNumberOfOddsEaqualToNumberOfEvens = countOdds(stones);

            if (stones.Length == 1)
            {
                // returns 0 when pile has a single stone
                return 0;
            }

            if (isNotEqual || !isNumberOfOddsEaqualToNumberOfEvens)
            {
                // returns -1  when piles can't have same no. of stones
                // Or when even numbers count is not equal to odd numbers count e.g {2, 4, 3}
                return -1;
            }

            else
            {
                int iteration = 0;

                //stones = stones.Except(new int[] { avg }).ToArray();

                for (int i = 0; i < stones.Length - 1; i++)
                {
                    if (stones[0] <= stones[stones.Length - 1])
                    {
                        Array.Sort(stones);
                        stones[0] = stones[0] + 2;
                        stones[stones.Length - 1] = stones[stones.Length - 1] - 2;
                        //stones = stones.Except(new int[] { avg }).ToArray();
                        iteration++;
                    }
                    else
                    {
                        Console.WriteLine("Can't make equal!");
                    }
                }
                return iteration;
            }
        }
        #region CommentedCode
        //public class GameOfStones2
        //{
        //    public static bool IsQuantized(List<int> items)
        //    {
        //        if (items == null || items.Count == 0)
        //            return true;

        //        return items.Cast<int>().All(x => x == 9);
        //    }


        //} 
        #endregion
    }
}